/**
 * Copyright (c) 2013, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlType(propOrder = {"devices", "mqttClientConfiguration"})
public class Configuration {

    private static final Logger LOGGER = LogManager.getLogger(Configuration.class);
    public static final String CONFIG_FILE_PATH = System.getProperty("user.home") + "/sweethomehub";
    public static final String CONFIG_FILE_NAME = "sweethomehub-config.xml";
    private List<Device> devices;
    private MqttClientConfiguration mqttClientConfiguration;

    public static Configuration load() {
        if (!getConfigFile().exists()) {
            LOGGER.info("{} doesn''t exist. Creating it for you with default settings.", getConfigFile());
            createAndSaveDefaultConfig();
        }
        LOGGER.info("Loading configuration file: {}", getConfigFile());
        Configuration configuration = (Configuration) JAXB.unmarshal(getConfigFile(), Configuration.class);
        return configuration;
    }

    public void save() {
        JAXB.marshal(this, getConfigFile());
    }

    private static void createAndSaveDefaultConfig() {
        Configuration configuration = new Configuration();
        List devices = new ArrayList();
        devices.add(new Device(SweethomeTopics.ACTOR_OUTDOOR_FRONT_MAINENTRANCE_LIGHT.getName(), "Light Front Door", "a", "1", "1"));
        devices.add(new Device(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_TERRACE_LIGHT.getName(), "Light Terrace", "a", "1", "2"));
        devices.add(new Device(SweethomeTopics.ACTOR_INDOOR_LIVINGROOM_WALL_LIGHT.getName(), "Light Livingroom", "a", "1", "3"));
        devices.add(new Device(SweethomeTopics.ACTOR_OUTDOOR_FRONT_WALK_LIGHT.getName(), "Light Garden", "a", "1", "4"));
        devices.add(new Device(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_FOUNTAIN.getName(), "Fountain", "a", "1", "5"));
        MqttClientConfiguration mqttClientConfiguration = new MqttClientConfiguration();
        mqttClientConfiguration.setMqttBrokerAddress("127.0.0.1");
        mqttClientConfiguration.setMqttMessagesBaseTopic(SweethomeTopics.BASE_TOPIC.getName());
        configuration.setMqttClientConfiguration(mqttClientConfiguration);
        configuration.setDevices(devices);
        configuration.save();
    }

    public static File getConfigFile() {
        return new File(CONFIG_FILE_PATH +"/" + CONFIG_FILE_NAME
    

    );
    }

    public MqttClientConfiguration getMqttClientConfiguration() {
        return this.mqttClientConfiguration;
    }

    public void setMqttClientConfiguration(MqttClientConfiguration mqttClientConfiguration) {
        this.mqttClientConfiguration = mqttClientConfiguration;
    }

    @XmlElementWrapper
    @XmlElement(name = "device")
    public List<Device> getDevices() {
        return this.devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

}
