/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlType(propOrder = {"houseCode", "groupId", "deviceId", "name", "mqttTopic"})
public class Device
{
  private String mqttTopic;
  private String name;
  private String houseCode;
  private String groupId;
  private String deviceId;

  public Device()
  {
  }

  public Device(String mqttTopic, String name, String houseCode, String group, String device)
  {
    this.mqttTopic = mqttTopic;
    this.name = name;
    this.houseCode = houseCode;
    this.groupId = group;
    this.deviceId = device;
  }

  public String getMqttTopic() {
    return this.mqttTopic;
  }

  public void setMqttTopic(String mqttTopic) {
    this.mqttTopic = mqttTopic;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDeviceId() {
    return this.deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public String getGroupId() {
    return this.groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getHouseCode() {
    return this.houseCode;
  }

  public void setHouseCode(String houseCode) {
    this.houseCode = houseCode;
  }

  public String getIntertechnoId() {
    return String.format("%s %s %s", new Object[] { this.houseCode, this.groupId, this.deviceId });
  }
  
  @Override
  public String toString(){
    return String.format("%s: %s %s %s", new Object[] { this.name, this.houseCode, this.groupId, this.deviceId });
  }
}