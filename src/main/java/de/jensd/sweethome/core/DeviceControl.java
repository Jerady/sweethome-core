/**
 * Copyright (c) 2013, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jens Deters
 */
public class DeviceControl {

    private final Send send = new Send();
    private static final Logger LOGGER = LogManager.getLogger(Configuration.class);

    public String turnOn(Device device) {
        return doSwitch(device, Send.Command.TURN_ON);
    }

    public String turnOff(Device device) {
        return doSwitch(device, Send.Command.TURN_OFF);
    }

    public String doSwitch(Device device, Send.Command command) {
        LOGGER.info("About to {} {} ({})", command, device.getName(), device.getIntertechnoId());
        if (send.isSendCommandExecutable()) {
            return send.send(device.getIntertechnoId(), command);
        }
        String errorMsg = String.format("Can't %s '%s': Not running on a Rapsberry Pi?", command, device.getName());
        LOGGER.error(errorMsg);
        return errorMsg;
    }

    public String doSwitch(DeviceCommand deviceCommand) {
        if (deviceCommand.getCommand().isPresent() && deviceCommand.getCommand().isPresent()) {
            return doSwitch(deviceCommand.getDevice().get(), deviceCommand.getCommand().get());
        }
        return "deviceCommand can not be executed.";
    }
}
