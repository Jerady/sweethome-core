/**
 * Copyright (c) 2014, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlType(propOrder = {"mqttClientId", "mqttBrokerAddress", "mqttBrokerPort", "mqttMessagesBaseTopic"})
public class MqttClientConfiguration {

    private StringProperty mqttClientIdProperty;
    private StringProperty mqttBrokerAddressProperty;
    private StringProperty mqttBrokerPortProperty;
    private StringProperty mqttMessagesBaseTopicProperty;

    public MqttClientConfiguration() {
        setMqttMessagesBaseTopic("sweethome");
        setMqttBrokerPort("1883");
        setMqttBrokerAddress("sweethome");
        setMqttClientId("SweethomeMQTTClientId");
   }

    public final StringProperty mqttMessagesBaseTopicProperty() {
        if (mqttMessagesBaseTopicProperty == null) {
            mqttMessagesBaseTopicProperty = new SimpleStringProperty();
        }
        return mqttMessagesBaseTopicProperty;
    }

    public final String getMqttMessagesBaseTopic() {
        return (String) mqttMessagesBaseTopicProperty().getValue();
    }

    public final void setMqttMessagesBaseTopic(String mqttMessagesBaseTopic) {
        mqttMessagesBaseTopicProperty().setValue(mqttMessagesBaseTopic);
    }

    public final StringProperty mqttClientIdProperty() {
        if (mqttClientIdProperty == null) {
            mqttClientIdProperty = new SimpleStringProperty();
        }
        return mqttClientIdProperty;
    }

    public final String getMqttClientId() {
        return mqttClientIdProperty().getValue();
    }

    public final void setMqttClientId(String mqttClientId) {
        mqttClientIdProperty().setValue(mqttClientId);
    }

    public final StringProperty mqttBrokerAddressProperty() {
        if (mqttBrokerAddressProperty == null) {
            mqttBrokerAddressProperty = new SimpleStringProperty();
        }
        return mqttBrokerAddressProperty;
    }

    public final String getMqttBrokerAddress() {
        return mqttBrokerAddressProperty().getValue();
    }

    public final void setMqttBrokerAddress(String mqttBrokerAddress) {
        mqttBrokerAddressProperty().setValue(mqttBrokerAddress);
    }

    public final StringProperty mqttBrokerPortProperty() {
        if (mqttBrokerPortProperty == null) {
            mqttBrokerPortProperty = new SimpleStringProperty();
        }
        return mqttBrokerPortProperty;
    }

    public final String getMqttBrokerPort() {
        return mqttBrokerPortProperty().getValue();
    }

    public final void setMqttBrokerPort(String mqttBrokerPort) {
        mqttBrokerPortProperty().setValue(mqttBrokerPort);
    }

    public final String getMqttBrokerURI() {
        return String.format("tcp://%s:%s", new Object[]{getMqttBrokerAddress(), getMqttBrokerPort()});
    }
}
