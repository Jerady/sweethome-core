/**
 * Copyright (c) 2015, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Jens Deters
 */
public class MqttClientController {

    private static final Logger LOGGER = LogManager.getLogger(MqttClientController.class);
    private StringProperty feedback;
    private BooleanProperty clientConnected;
    private ObjectProperty<MqttClient> mqttClient;
    private final MqttClientConfiguration mqttClientConfiguration;
    private MqttClientPersistence persistence;

    public MqttClientController(MqttClientConfiguration mqttClientConfiguration) {
        this.mqttClientConfiguration = mqttClientConfiguration;
    }

    public MqttClientConfiguration getMqttClientConfiguration() {
        return mqttClientConfiguration;
    }
    
    public MqttClientPersistence getPersistence() {
        if (this.persistence == null) {
            this.persistence = new MemoryPersistence();
        }
        return this.persistence;
    }

    public StringProperty feedbackProperty() {
        if (this.feedback == null) {
            this.feedback = new SimpleStringProperty();
        }
        return this.feedback;
    }

    public void setFeedback(String message) {
        feedbackProperty().setValue(message);
    }

    public String getFeedback() {
        return feedbackProperty().getValue();
    }

    public ObjectProperty<MqttClient> mqttClientProperty() {
        if (this.mqttClient == null) {
            this.mqttClient = new SimpleObjectProperty();
        }
        return this.mqttClient;
    }

    public void setMqttClient(MqttClient mqttClient) {
        mqttClientProperty().set(mqttClient);
    }

    public MqttClient getMqttClient() {
        return (MqttClient) mqttClientProperty().get();
    }

    public BooleanProperty clientConnectedProperty() {
        if (this.clientConnected == null) {
            this.clientConnected = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return this.clientConnected;
    }

    public void setClientConnected(Boolean clientConnected) {
        clientConnectedProperty().set(clientConnected);
    }

    public boolean isClientConnected() {
        if (getMqttClient() != null) {
            return getMqttClient().isConnected();
        }
        return false;
    }

    public void connect() throws MqttException {
        if ((getMqttClient() == null) || (!getMqttClient().isConnected())) {
            LOGGER.info("Connect...");
            setFeedback("Connect...");
            String clientID = this.mqttClientConfiguration.getMqttClientId();
            String brokerURI = this.mqttClientConfiguration.getMqttBrokerURI();
            LOGGER.info("Connecting to: {} with clientID: {}", new Object[]{brokerURI, clientID});
            setMqttClient(new MqttClient(brokerURI, clientID, this.persistence));
            getMqttClient().connect();
            setClientConnected(Boolean.TRUE);
            setFeedback("Connected.");
        }
    }

    public void disconnect() throws MqttException {
        if ((getMqttClient() != null) && (getMqttClient().isConnected())) {
            LOGGER.info("Disconnect...");
            setFeedback("Disconnect");
            getMqttClient().disconnect();
            setClientConnected(Boolean.FALSE);
            setFeedback("Disconnected.");
        }
    }
}
