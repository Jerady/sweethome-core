package de.jensd.sweethome.core;

import java.time.LocalDateTime;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class SweetHomeValues {
    
    private DoubleProperty outdoorBackyardTemperature;
    private DoubleProperty indoorLivingroomTemperature;
    private DoubleProperty indoorBathroomTemperature;
    private DoubleProperty indoorBathroomHumidity;
    private DoubleProperty indoorLivingroomHumidity;
    private DoubleProperty outdoorBackyardPressure;
    private IntegerProperty outdoorBackyardLuxVisible;
    private IntegerProperty outdoorBackyardLuxInfrared;
    private IntegerProperty outdoorBackyardUvIndex;
    private ObjectProperty<LocalDateTime> lastUpdate;
    private DoubleProperty energyConsumption;
    private DoubleProperty currentIrms;
    
    public SweetHomeValues() {
        resetValues();
    }
    
    public final void resetValues() {
        setOutdoorBackyardTemperature(0.0);
        setIndoorLivingroomTemperature(0.0);
        setOutdoorBackyardPressure(0.0);
        setIndoorLivingroomHumidity(0.0);
        setOutdoorBackyardLuxInfrared(0);
        setOutdoorBackyardLuxVisible(0);
    }
    
    public DoubleProperty indoorBathroomTemperatureProperty() {
        if (indoorBathroomTemperature == null) {
            indoorBathroomTemperature = new SimpleDoubleProperty();
        }
        return indoorBathroomTemperature;
    }
    
    public Double getIndoorBathroomTemperature() {
        return indoorBathroomTemperatureProperty().getValue();
    }
    
    public void setIndoorBathroomTemperature(Double lux) {
        indoorBathroomTemperatureProperty().setValue(lux);
        setLastUpdate(LocalDateTime.now());
    }
    
    public DoubleProperty indoorBathroomHumidityProperty() {
        if (indoorBathroomHumidity == null) {
            indoorBathroomHumidity = new SimpleDoubleProperty();
        }
        return indoorBathroomHumidity;
    }
    
    public Double getIndoorBathroomHumidity() {
        return indoorBathroomHumidityProperty().getValue();
    }
    
    public void setIndoorBathroomHumidity(Double lux) {
        indoorBathroomHumidityProperty().setValue(lux);
        setLastUpdate(LocalDateTime.now());
    }
    
    public IntegerProperty outdoorBackyardLuxInfraredProperty() {
        if (outdoorBackyardLuxInfrared == null) {
            outdoorBackyardLuxInfrared = new SimpleIntegerProperty();
        }
        return outdoorBackyardLuxInfrared;
    }
    
    public Integer getOutdoorBackyardLuxInfrared() {
        return outdoorBackyardLuxInfraredProperty().getValue();
    }
    
    public void setOutdoorBackyardLuxInfrared(Integer lux) {
        outdoorBackyardLuxInfraredProperty().setValue(lux);
        setLastUpdate(LocalDateTime.now());
    }
    
    public IntegerProperty outdoorBackyardLuxVisibleProperty() {
        if (outdoorBackyardLuxVisible == null) {
            outdoorBackyardLuxVisible = new SimpleIntegerProperty();
        }
        return outdoorBackyardLuxVisible;
    }
    
    public Integer getOutdoorBackyardLuxVisible() {
        return outdoorBackyardLuxVisibleProperty().getValue();
    }
    
    public void setOutdoorBackyardLuxVisible(Integer lux) {
        outdoorBackyardLuxVisibleProperty().setValue(lux);
        setLastUpdate(LocalDateTime.now());
    }
    
    public DoubleProperty outdoorBackyardPressureProperty() {
        if (outdoorBackyardPressure == null) {
            outdoorBackyardPressure = new SimpleDoubleProperty();
        }
        return outdoorBackyardPressure;
    }
    
    public Double getOutdoorBackyardPressure() {
        return outdoorBackyardPressureProperty().getValue();
    }
    
    public void setOutdoorBackyardPressure(Double pressure) {
        outdoorBackyardPressureProperty().setValue(pressure);
        setLastUpdate(LocalDateTime.now());
    }
    
    public DoubleProperty indoorLivingroomTemperatureProperty() {
        if (indoorLivingroomTemperature == null) {
            indoorLivingroomTemperature = new SimpleDoubleProperty();
        }
        return indoorLivingroomTemperature;
    }
    
    public Double getIndoorLivingroomTemperature() {
        return indoorLivingroomTemperatureProperty().getValue();
    }
    
    public void setIndoorLivingroomTemperature(Double temperature) {
        indoorLivingroomTemperatureProperty().setValue(temperature);
        setLastUpdate(LocalDateTime.now());
    }
    
    public DoubleProperty indoorLivingroomHumidityProperty() {
        if (indoorLivingroomHumidity == null) {
            indoorLivingroomHumidity = new SimpleDoubleProperty();
        }
        return indoorLivingroomHumidity;
    }
    
    public Double getIndoorLivingroomHumidity() {
        return indoorLivingroomHumidityProperty().getValue();
    }
    
    public void setIndoorLivingroomHumidity(Double humidity) {
        indoorLivingroomHumidityProperty().setValue(humidity);
        setLastUpdate(LocalDateTime.now());
    }
    
    public DoubleProperty outdoorBackyardTemperatureProperty() {
        if (outdoorBackyardTemperature == null) {
            outdoorBackyardTemperature = new SimpleDoubleProperty();
        }
        return outdoorBackyardTemperature;
    }
    
    public Double getOutdoorBackyardTemperature() {
        return outdoorBackyardTemperatureProperty().getValue();
    }
    
    public void setOutdoorBackyardTemperature(Double temperature) {
        outdoorBackyardTemperatureProperty().setValue(temperature);
        setLastUpdate(LocalDateTime.now());
    }
    
    public ObjectProperty<LocalDateTime> lastUpdateProperty() {
        if (lastUpdate == null) {
            lastUpdate = new SimpleObjectProperty<>();
        }
        return lastUpdate;
    }
    
    public LocalDateTime getLastUpdate() {
        return lastUpdateProperty().getValue();
    }
    
    public void setLastUpdate(LocalDateTime lastUpdate) {
        lastUpdateProperty().setValue(lastUpdate);
    }
    
    public IntegerProperty outdoorBackyardUvIndexProperty() {
        if (outdoorBackyardUvIndex == null) {
            outdoorBackyardUvIndex = new SimpleIntegerProperty(1);
        }
        return outdoorBackyardUvIndex;
    }
    
    public Integer getOutdoorBackyardUvIndex() {
        return outdoorBackyardUvIndexProperty().get();
    }
    
    public void setOutdoorBackyardUvIndex(Integer outdoorBackyardUvIndex) {
        outdoorBackyardUvIndexProperty().set(outdoorBackyardUvIndex);
    }
    
    public DoubleProperty energyConsumptionProperty() {
        if (energyConsumption == null) {
            energyConsumption = new SimpleDoubleProperty();
        }
        return energyConsumption;
    }
    
    public Double getEnergyConsumption() {
        return energyConsumptionProperty().get();
    }
    
    public void setEnergyConsumption(Double value) {
        energyConsumptionProperty().set(value);
    }
    
    public DoubleProperty currentIrmsProperty() {
        if (currentIrms == null) {
            currentIrms = new SimpleDoubleProperty();
        }
        return currentIrms;
    }
    
    public Double getCurrentIrms() {
        return currentIrmsProperty().get();
    }
    
    public void setCurrentIrms(Double value) {
        currentIrmsProperty().set(value);
    }
    
}
