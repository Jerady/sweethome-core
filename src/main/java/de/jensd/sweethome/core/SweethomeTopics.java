/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.sweethome.core;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public enum SweethomeTopics {

    BASE_TOPIC("sweethome"),
    SENSOR_INDOOR_LIVINGROOM_TEMPERATURE("sensor/indoor/livingroom/temperature"),
    SENSOR_INDOOR_LIVINGROOM_HUMIDITY("sensor/indoor/livingroom/humidity"),
    SENSOR_INDOOR_LIVINGROOM_PRESSURE("sensor/indoor/livingroom/pressure"),
    SENSOR_INDOOR_BATHROOM_TEMPERATURE("sensor/indoor/bathroom/temperature"),
    SENSOR_INDOOR_BATHROOM_HUMIDITY("sensor/indoor/bathroom/humidity"),
    SENSOR_INDOOR_OVERALL_ENERGY_CONSUMPTION("sensor/indoor/overall/energyconsumption"),
    SENSOR_INDOOR_OVERALL_IRMS("sensor/indoor/overall/irms"),
    SENSOR_OUTDOOR_BACKYARD_UVINDEX("sensor/outdoor/backyard/uvindex"),
    SENSOR_OUTDOOR_BACKYARD_TEMPERATURE("sensor/outdoor/backyard/temperature"),
    SENSOR_OUTDOOR_BACKYARD_HUMIDITY("sensor/outdoor/backyard/humidity"),
    SENSOR_OUTDOOR_BACKYARD_PRESSURE("sensor/outdoor/backyard/pressure"),
    SENSOR_OUTDOOR_BACKYARD_ALTITUDE("sensor/outdoor/backyard/altitude"),
    SENSOR_OUTDOOR_BACKYARD_LUX_VISIBLE("sensor/outdoor/backyard/lux"),
    SENSOR_OUTDOOR_BACKYARD_LUX_INFRARED("sensor/outdoor/backyard/lux-ir"),
    ACTOR_OUTDOOR_FRONT_MAINENTRANCE_LIGHT("actor/outdoor/front/mainentrance/light"),
    ACTOR_OUTDOOR_FRONT_WALK_LIGHT("actor/outdoor/front/walk/light"),
    ACTOR_OUTDOOR_BACKYARD_TERRACE_LIGHT("actor/outdoor/backyard/terrace/light"),
    ACTOR_OUTDOOR_BACKYARD_GREENAREA_LIGHT("actor/outdoor/backyard/greenarea/light"),
    ACTOR_OUTDOOR_BACKYARD_FOUNTAIN("actor/outdoor/backyard/fountain"),
    ACTOR_INDOOR_LIVINGROOM_WALL_LIGHT("actor/indoor/livingroom/wall/light"),
    ACTOR_INDOOR_LIVINGROOM_GECKO_LIGHT("actor/indoor/livingroom/gecko/light"),
    ACTOR_INDOOR_LIVINGROOM_TV_LIGHT("actor/indoor/livingroom/tv/light"),
    JSON_ACTORS_COMMAND("actors/jsoncommand");

    private final String name;

    private SweethomeTopics(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public String getNameWithDefaultBaseTopic() {
        return String.format("%s/%s", BASE_TOPIC.getName(),name);
    }
    

}
