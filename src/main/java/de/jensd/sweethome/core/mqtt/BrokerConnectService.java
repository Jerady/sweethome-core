package de.jensd.sweethome.core.mqtt;

import de.jensd.sweethome.core.MqttClientController;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 *
 * @author Jens Deters
 */
public class BrokerConnectService extends Service<Boolean> {

    private final static Logger LOGGER = Logger.getLogger(BrokerConnectService.class.getName());
    private final MqttClientController mqttClientController;
    private StringProperty feedback;

    public BrokerConnectService(MqttClientController mqttClientController) {
        this.mqttClientController = mqttClientController;
    }

    public StringProperty feedbackProperty() {
        if (feedback == null) {
            feedback = new SimpleStringProperty();
        }
        return feedback;
    }

    private void setFeedback(String feedback) {
        feedbackProperty().set(feedback);
    }

    public String getFeedback() {
        return feedbackProperty().get();
    }

    @Override
    protected void failed() {
        String message;
        if (getException().getCause() != null) {
            message = getException().getCause().getClass().getSimpleName() + ": " + getException().getCause().getMessage();
        } else {
            message = getException().getClass().getSimpleName() + ": " + getException().getMessage();
        }
        setFeedback(message);
        LOGGER.severe(message);
    }

    @Override
    protected Task<Boolean> createTask() {
        return new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                mqttClientController.connect();
                return mqttClientController.isClientConnected();
            }
        };
    }
}
