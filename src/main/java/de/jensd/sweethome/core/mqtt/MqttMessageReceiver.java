/**
 * Copyright (c) 2015, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core.mqtt;

import de.jensd.sweethome.core.MqttClientController;
import de.jensd.sweethome.core.Configuration;
import de.jensd.sweethome.core.Device;
import de.jensd.sweethome.core.DeviceCommand;
import de.jensd.sweethome.core.DeviceControl;
import de.jensd.sweethome.core.Send;
import de.jensd.sweethome.core.SweethomeTopics;
import de.jensd.sweethome.core.json.JsonDeviceCommandProcessor;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javax.json.Json;
import javax.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author Jens Deters
 */
public class MqttMessageReceiver implements MqttCallback {

    private static final Logger LOGGER = LogManager.getLogger(MqttMessageReceiver.class);
    private final MqttClientController mqttClientController;
    private final Configuration configuration;
    private Map<String, Device> deviceMap;
    private final DeviceControl deviceControl;
    private final String jsonMessageTopic;
    private BooleanProperty connected;

    public MqttMessageReceiver(Configuration configuration) {
        mqttClientController = new MqttClientController(configuration.getMqttClientConfiguration());
        this.configuration = configuration;
        deviceControl = new DeviceControl();
        deviceMap = new HashMap();
        jsonMessageTopic = String.format("%s/%s", configuration.getMqttClientConfiguration().getMqttMessagesBaseTopic(), SweethomeTopics.JSON_ACTORS_COMMAND.getName());
        configuration.getDevices().stream().forEach((device) -> {
            String deviceTopic = String.format("%s/%s", new Object[]{configuration.getMqttClientConfiguration().getMqttMessagesBaseTopic(), device.getMqttTopic()});
            deviceMap.put(deviceTopic, device);
        });
    }

    public void start() throws MqttException {
        mqttClientController.connect();
        setConnected(mqttClientController.isClientConnected());
        mqttClientController.getMqttClient().setCallback(this);
        LOGGER.info("- - - - - - - - - - - - - - -");
        LOGGER.info("-  MQTT Receiver (Switch)   -");
        LOGGER.info("- - - - - - - - - - - - - - -");
        LOGGER.info("Started.");
        LOGGER.info("Configured actors:");
        configuration.getDevices().forEach(d -> {
            LOGGER.info(configuration.getMqttClientConfiguration().getMqttMessagesBaseTopic() + "/" + d.getMqttTopic());
        });
        LOGGER.info("Send JSON commands to " + jsonMessageTopic);
        String baseTopic = String.format("%s/actor/#", new Object[]{configuration.getMqttClientConfiguration().getMqttMessagesBaseTopic()});
        mqttClientController.getMqttClient().subscribe(baseTopic);
        LOGGER.info("Subscribed to actors base topic: " + baseTopic);
        LOGGER.info("Waiting for incoming requests...");
    }

    public void stop() throws MqttException {
        LOGGER.info("Shutdown was requested.");
        mqttClientController.disconnect();
        setConnected(mqttClientController.isClientConnected());
        LOGGER.info("Halted.");
    }

    @Override
    public void connectionLost(Throwable cause) {
        LOGGER.error("Lost connection to MQTT Broker!");
       while (!mqttClientController.isClientConnected()) {
        try {
            LOGGER.info("Retrying to connect in 5s...");
            Thread.sleep(5000);
            try {
                start();
            } catch (MqttException ex) {
                LOGGER.info("MQTT Broker still seems to be down.");
            }
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(MqttMessageReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        try {
            String payload = new String(message.getPayload());
            LOGGER.info("Incoming message for topic '{}': {}", topic, message);
            if (deviceMap.containsKey(topic)) {
                Device device = (Device) this.deviceMap.get(topic);
                Send.Command command = Send.Command.TURN_OFF;
                if (payload.contains("ON")) {
                    command = Send.Command.TURN_ON;
                }
                deviceControl.doSwitch(device, command);
            } else if (jsonMessageTopic.equals(topic)) {
                StringReader reader = new StringReader(payload);
                JsonObject jsonObject = Json.createReader(reader).readObject();
                List<DeviceCommand> deviceCommands = JsonDeviceCommandProcessor.parse(jsonObject);
                deviceCommands.stream().forEach((deviceCommand) -> {
                    deviceControl.doSwitch(deviceCommand);
                });
            }
        } catch (Exception e) {
        }
    }

    public BooleanProperty connectedProperty() {
        if (connected == null) {
            connected = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return connected;
    }

    public Boolean isConnected() {
        return connectedProperty().getValue();
    }

    public void setConnected(Boolean connected) {
        connectedProperty().setValue(connected);
    }

}
