package de.jensd.sweethome.core.mqtt;

import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.MqttClientController;
import de.jensd.sweethome.core.SweetHomeValues;
import de.jensd.sweethome.core.SweethomeTopics;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class SweethomeValuesMQTTAdapter implements MqttCallback {

    public enum ConnectionStatus {

        UNDEFINED, CONNECTING, DISCONNECTED, CONNECTED;
    }

    private static final Logger LOGGER = LogManager.getLogger(SweethomeValuesMQTTAdapter.class);
    private static final DateFormat TIME = new SimpleDateFormat("hh:mm:ss.sss");
    private final MqttClientController mqttClientController;
    private final SweetHomeValues valuesDataModel;
    private List<SweethomeTopics> subscribeTopics;
    private ObjectProperty<ConnectionStatus> connectionStatus;
    private StringProperty lastMessageArrived;

    public SweethomeValuesMQTTAdapter(MqttClientConfiguration mqttClientConfiguration, SweetHomeValues valuesDataModel) {
        this.valuesDataModel = valuesDataModel;
        mqttClientController = new MqttClientController(mqttClientConfiguration);
        init();
    }

    private void init() {
        subscribeTopics = new ArrayList<>();
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_TEMPERATURE);
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_PRESSURE);
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_ALTITUDE);
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_LUX_VISIBLE);
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_LUX_INFRARED);
        subscribeTopics.add(SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_UVINDEX);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_LIVINGROOM_HUMIDITY);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_LIVINGROOM_TEMPERATURE);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_BATHROOM_HUMIDITY);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_BATHROOM_TEMPERATURE);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_OVERALL_ENERGY_CONSUMPTION);
        subscribeTopics.add(SweethomeTopics.SENSOR_INDOOR_OVERALL_IRMS);

        connectionStatusProperty().addListener((ObservableValue<? extends ConnectionStatus> observable, ConnectionStatus oldValue, ConnectionStatus connectedValue) -> {
            if (connectedValue == ConnectionStatus.CONNECTED) {
                subscribe();
            }
        });
    }

    public final void subscribe() {
        mqttClientController.getMqttClient().setCallback(this);
        LOGGER.info("- - - - - - - - - - - - - - - -");
        LOGGER.info("       S W E E T H O M E      -");
        LOGGER.info("     MQTT  Values Receiver    -");
        LOGGER.info("- - - - - - - - - - - - - - - -");
        LOGGER.info("Started.");
        LOGGER.info("Subscribing to measurement topics:");
        for (SweethomeTopics t : subscribeTopics) {
            try {
                mqttClientController.getMqttClient().subscribe(t.getNameWithDefaultBaseTopic());
                LOGGER.info("Subscribed to " + t.getNameWithDefaultBaseTopic());
            } catch (MqttException ex) {
                LOGGER.throwing(ex);
            }
        }
        LOGGER.info("Waiting for incoming requests...");
    }

    public void reconnect() throws MqttException {
        LOGGER.info("reconnect()");
        disconnect();
        connect();
    }

    public void connect() throws MqttException {
        LOGGER.info("connect()");
        if (isClientConnected()) {
            LOGGER.info("Client is already connected: doing nothing.");
            return;
        }
        LOGGER.info("Connecting...");
        Task<Boolean> task = createConnectTask();
        task.setOnSucceeded((WorkerStateEvent event) -> {
            LOGGER.info("Connectied.");
            Platform.runLater(() -> {
                setConnectionStatusProperty(ConnectionStatus.CONNECTED);
            });
        });
        task.setOnFailed((WorkerStateEvent event) -> {
            LOGGER.info("Connection failed.");
            Platform.runLater(() -> {
                setConnectionStatusProperty(ConnectionStatus.DISCONNECTED);
            });
        });
        task.setOnRunning((WorkerStateEvent event) -> {
            LOGGER.info("Connecting...");
            Platform.runLater(() -> {
                setConnectionStatusProperty(ConnectionStatus.CONNECTING);
            });
        });
        new Thread(task).start();
    }

    public void disconnect() throws MqttException {
        LOGGER.info("disconnect()");
        if (!isClientConnected()) {
            LOGGER.info("Client is not connected: doing nothing.");
            return;
        }
        LOGGER.info("Disconnecting...");
        mqttClientController.disconnect();
        setConnectionStatusProperty(ConnectionStatus.DISCONNECTED);
        setConnected(false);
        LOGGER.info("Disconnected.");
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
        setConnectionStatusProperty(ConnectionStatus.DISCONNECTED);
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        LOGGER.info("Incoming message for topic '{}': {}", topic, message);
        setLastMessageArrived(String.format("%s - %s: %s%n", TIME.format(new Date()), topic, message));
        if (SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_TEMPERATURE.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setOutdoorBackyardTemperature(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_LIVINGROOM_HUMIDITY.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setIndoorLivingroomHumidity(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_LIVINGROOM_TEMPERATURE.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setIndoorLivingroomTemperature(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_OVERALL_ENERGY_CONSUMPTION.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setEnergyConsumption(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_OVERALL_IRMS.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setCurrentIrms(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_PRESSURE.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setOutdoorBackyardPressure(getDoubleValue(message)); //hPa
            });
        } else if (SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_LUX_VISIBLE.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setOutdoorBackyardLuxVisible(getIntegerValue(message));
            });
        } else if (SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_LUX_INFRARED.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setOutdoorBackyardLuxInfrared(getIntegerValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_BATHROOM_TEMPERATURE.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setIndoorBathroomTemperature(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_INDOOR_BATHROOM_HUMIDITY.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setIndoorBathroomHumidity(getDoubleValue(message));
            });
        } else if (SweethomeTopics.SENSOR_OUTDOOR_BACKYARD_UVINDEX.getNameWithDefaultBaseTopic().equals(topic)) {
            Platform.runLater(() -> {
                valuesDataModel.setOutdoorBackyardUvIndex(getIntegerValue(message));
            });
        }
    }

    public MqttClientConfiguration getMqttClientConfiguration() {
        return mqttClientController.getMqttClientConfiguration();
    }

    private String cleanupValue(String rawPayload) {
        return rawPayload.replace(',', '.').replaceAll("\\s+", "");
    }

    private Double getDoubleValue(MqttMessage message) {
        String payload = cleanupValue(new String(message.getPayload()));
        double d = 0.0;
        try {
            d = Double.valueOf(payload);
        } catch (Exception ex) {
        }
        return d;
    }

    private Integer getIntegerValue(MqttMessage message) {
        String payload = cleanupValue(new String(message.getPayload()));
        int i = 0;
        try {
            i = Integer.valueOf(payload);
        } catch (Exception ex) {
        }
        return i;
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        LOGGER.info("deliveryComplete.");
    }

    public boolean isClientConnected() {
        return mqttClientController.isClientConnected();
    }

    public void publish(String topic, String payload) {
        MqttMessage message = new MqttMessage(payload.getBytes());
        try {
            LOGGER.info("Publish " + payload + " to " + topic);
            mqttClientController.getMqttClient().publish(topic, message);
        } catch (MqttException ex) {
            LOGGER.throwing(ex);
        }
    }

    public BooleanProperty connectedProperty() {
        return mqttClientController.clientConnectedProperty();
    }

    public void setConnected(Boolean connected) {
        mqttClientController.clientConnectedProperty().set(connected);
    }

    public Boolean getConnected() {
        return mqttClientController.clientConnectedProperty().get();
    }

    public ObjectProperty<ConnectionStatus> connectionStatusProperty() {
        if (connectionStatus == null) {
            connectionStatus = new SimpleObjectProperty<>(ConnectionStatus.UNDEFINED);
        }
        return connectionStatus;
    }

    public void setConnectionStatusProperty(ConnectionStatus connectionStatus) {
        connectionStatusProperty().setValue(connectionStatus);
    }

    public ConnectionStatus isConnectionStatusProperty() {
        return connectionStatusProperty().getValue();
    }

    public StringProperty lastMessageArrivedProperty() {
        if (lastMessageArrived == null) {
            lastMessageArrived = new SimpleStringProperty();
        }
        return lastMessageArrived;
    }

    public String getLastMessageArrived() {
        return lastMessageArrivedProperty().get();
    }

    public void setLastMessageArrived(String lastMessageArrived) {
        lastMessageArrivedProperty().set(lastMessageArrived);
    }

    protected Task<Boolean> createConnectTask() {
        return new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                mqttClientController.connect();
                return mqttClientController.isClientConnected();
            }
        };
    }

}
