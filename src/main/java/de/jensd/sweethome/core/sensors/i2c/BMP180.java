package de.jensd.sweethome.core.sensors.i2c;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jens Deters
 */
public class BMP180 {

    //Callibration Data
    private static final int EEPROM_start = 0xAA;
    private static final int EEPROM_end = 0xBF;

    // EEPROM registers - these represent calibration data
    private short AC1;
    private short AC2;
    private short AC3;
    private int AC4;
    private int AC5;
    private int AC6;
    private short B1;
    private short B2;
    private short MB;
    private short MC;
    private short MD;

    //Variable common between temperature & pressure calculations
    private int B5;

    // Device address 
    private static final int DEVICE_ADDRESS = 0x77;
    // Temperature Control Register Data
    private static final int CONTROL_REGISTER = 0xF4;
    private static final int CALLIBRATION_BYTES = 22;
    // Temperature read address
    private static final byte TEMPERATUR_ADDRESS = (byte) 0xF6;
    // Read temperature command
    private static final byte GET_TEMPERATURE_COMMAND = (byte) 0x2E;
    // Pressure read address
    private static final byte PRESSURE_ADDRESS = (byte) 0xF6;
    // Read Pressure command
    private static final byte GET_PRESSURE_COMMAND = (byte) 0x34;
    // oversampling_setting
    private static final byte OSS = (byte) 0;

    //I2C bus
    I2CBus bus;
    // Device object
    private I2CDevice bmp180;

    private DataInputStream bmp180CaliIn;
    private DataInputStream bmp180In;

    public void connect() {
        try {
            bus = I2CFactory.getInstance(I2CBus.BUS_1);
            System.out.println("Connected to bus OK!!!");
            //get device itself
            bmp180 = bus.getDevice(DEVICE_ADDRESS);
            System.out.println("Connected to device OK!!!");
            //Small delay before starting
            Thread.sleep(500);
            //Getting callibration data
            callibrate();
        } catch (IOException | InterruptedException e) {
            Logger.getLogger(BMP180.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private void callibrate() {
        try {
            byte[] bytes = new byte[CALLIBRATION_BYTES];

            //read all callibration data into byte array
            int readTotal = bmp180.read(EEPROM_start, bytes, 0, CALLIBRATION_BYTES);
            if (readTotal != 22) {
                System.out.println("Error bytes read: " + readTotal);
            }

            bmp180CaliIn = new DataInputStream(new ByteArrayInputStream(bytes));

            // Read each of the pairs of data as signed short
            AC1 = bmp180CaliIn.readShort();
            AC2 = bmp180CaliIn.readShort();
            AC3 = bmp180CaliIn.readShort();

            // Unsigned short Values
            AC4 = bmp180CaliIn.readUnsignedShort();
            AC5 = bmp180CaliIn.readUnsignedShort();
            AC6 = bmp180CaliIn.readUnsignedShort();

            //Signed sort values
            B1 = bmp180CaliIn.readShort();
            B2 = bmp180CaliIn.readShort();
            MB = bmp180CaliIn.readShort();
            MC = bmp180CaliIn.readShort();

            MD = bmp180CaliIn.readShort();

        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public void printCallibration() {
        String callibration = String.format("Callibration:%n AC1=%d%n AC2=%d%n AC3=%d%n AC4=%d%n AC5=%d%n AC6=%d%n B1=%d%n B2=%d%n MB=%d%n MC=%d%n MD=%d%n", AC1, AC2, AC3, AC4, AC5, AC6, B1, B2, MB, MC, MD);
        System.out.println(callibration);
    }

    /**
     * Calculated the temperature.
     *
     * @return the temperature in Celsius
     */
    public double readTemperature() {
        int uncompensatedTemperature = 0;
        byte[] bytesTemp = new byte[2];
        try {
            bmp180.write(CONTROL_REGISTER, GET_TEMPERATURE_COMMAND);
            Thread.sleep(5);
            int readTotal = bmp180.read(TEMPERATUR_ADDRESS, bytesTemp, 0, 2);
            if (readTotal < 2) {
                System.out.format("Error: %n bytes read/n", readTotal);
            }
            bmp180In = new DataInputStream(new ByteArrayInputStream(bytesTemp));
            uncompensatedTemperature = bmp180In.readUnsignedShort();
        } catch (IOException e) {
            System.out.println("Error reading temp: " + e.getMessage());
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception: " + e.getMessage());
        }

        //calculate temperature
        int X1 = ((uncompensatedTemperature - AC6) * AC5) >> 15;
        int X2 = (MC << 11) / (X1 + MD);
        B5 = X1 + X2;
        double celsius = ((B5 + 8) >> 4) * 0.1;
        return celsius;
    }

    /**
     * Calculated the current pressure.
     *
     * @return the pressure in Pascal
     */
    public long readPressure() {
        int uncompensatedPressure = 0;
        byte[] bytesPressure = new byte[3];
        try {
            bmp180.write(CONTROL_REGISTER, GET_PRESSURE_COMMAND);
            Thread.sleep(500);
            int readTotal = bmp180.read(PRESSURE_ADDRESS, bytesPressure, 0, 3);
            if (readTotal < 3) {
                System.out.format("Error: %n bytes read/n", readTotal);
            }
            bmp180In = new DataInputStream(new ByteArrayInputStream(bytesPressure));
            uncompensatedPressure = bmp180In.readUnsignedShort();
        } catch (IOException e) {
            System.out.println("Error reading pressure: " + e.getMessage());
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception: " + e.getMessage());
        }

        //calculate pressure
        long B6 = B5 - 4000;
        long X1 = (B2 * ((B6 * B6) >> 12) >> 11);
        long X2 = (AC2 * B6) >> 11;
        long X3 = X1 + X2;
        long B3 = (((AC1 * 4 + X3) << OSS) + 2) / 4;
        X1 = (AC3 * B6) >> 13;
        X2 = (B1 * ((B6 * B6) >> 12) >> 16);
        X3 = ((X1 + X2) + 2) >> 2;
        long B4 = AC4 * (long) (X3 + 32768) >> 15;
        long B7 = ((long) (uncompensatedPressure - B3)) * (50000 >> OSS);
        long pressure;
        if (B7 < 0x80000000) {
            pressure = (B7 * 2) / B4;
        } else {
            pressure = (B7 / B4) * 2;
        }
        X1 = (pressure >> 8) * (pressure >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * pressure) >> 16;
        pressure = pressure + ((X1 + X2 + 3791) >> 4);
        return pressure;
    }

    /**
     * Calculates the altitude in meters.
     *
     * @return the altitude in meters
     */
    public double readAltitude() {
        double sealevelPA = 101325.0;
        // Calculation taken straight from section 3.6 of the datasheet.pressure = float
        long pressure = readPressure();
        double altitude = 44330.0 * (1.0 - Math.pow(pressure / sealevelPA, (1.0 / 5.255)));
        return altitude;
    }

    /**
     * Calculates the pressure at sealevel when given a known altitude in meters.
     *
     * @return the sealevel pressure in Pascal
     */
    public double readSealevelPressure() {
        double altitudeM = 0.0;
        long pressure = readPressure();
        double sealevelPressure = pressure / Math.pow(1.0 - altitudeM / 44330.0, 5.255);
        return sealevelPressure;
    }

}
